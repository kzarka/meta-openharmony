# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

This patch is for //base/startup/init_lite repository of OpenHarmony 3.0

Write log output to stderr instead of /dev/kmsg, which can be directed to
systemd journal when run as a systemd service. This is for the param service,
which is a dependency of hilog, which we can therefore not log to.

Signed-off-by: Esben Haabendal <esben@geanix.com>
Upstream-Status: Pending

diff --git a/services/log/init_log.c b/services/log/init_log.c
index 852be3d43b89..7314c6fb8c40 100644
--- a/services/log/init_log.c
+++ b/services/log/init_log.c
@@ -17,6 +17,7 @@
 #include <errno.h>
 #include <fcntl.h>
 #include <stdarg.h>
+#include <unistd.h>
 #include <sys/stat.h>
 #include <time.h>
 #include "securec.h"
@@ -65,16 +66,6 @@ void InitToHiLog(LogLevel logLevel, const char *fmt, ...)
 }
 #endif
 
-static int g_fd = -1;
-void OpenLogDevice(void)
-{
-    int fd = open("/dev/kmsg", O_WRONLY | O_CLOEXEC, S_IRUSR | S_IWUSR | S_IRGRP | S_IRGRP);
-    if (fd >= 0) {
-        g_fd = fd;
-    }
-    return;
-}
-
 void EnableDevKmsg(void)
 {
     /* printk_devkmsg default value is ratelimit, We need to set "on" and remove the restrictions */
@@ -96,33 +87,20 @@ void InitLog(InitLogLevel logLevel, const char *fileName, int line, const char *
         return;
     }
 
-    if (UNLIKELY(g_fd < 0)) {
-        OpenLogDevice();
-        if (g_fd < 0) {
-            return;
-        }
-    }
     va_list vargs;
     va_start(vargs, fmt);
     char tmpFmt[MAX_LOG_SIZE];
     if (vsnprintf_s(tmpFmt, MAX_LOG_SIZE, MAX_LOG_SIZE - 1, fmt, vargs) == -1) {
-        close(g_fd);
-        g_fd = -1;
         return;
     }
 
     char logInfo[MAX_LOG_SIZE];
     if (snprintf_s(logInfo, MAX_LOG_SIZE, MAX_LOG_SIZE - 1, "%s[pid=%d][%s:%d][%s][%s] %s",
         kLevel, getpid(), fileName, line, INIT_LOG_TAG, LOG_LEVEL_STR[logLevel], tmpFmt) == -1) {
-        close(g_fd);
-        g_fd = -1;
         return;
     }
     va_end(vargs);
 
-    if (write(g_fd, logInfo, strlen(logInfo)) < 0) {
-        close(g_fd);
-        g_fd = -1;
-    }
+    write(STDERR_FILENO, logInfo, strlen(logInfo));
     return;
 }
