# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

xf86drm.c: Add drmWaitVBlank hack

DRM_IOCTL_WAIT_VBLANK is not supported by the virtio drm driver. This feature
is essential for the inner workings of the OpenHarmony graphic stack, therefore
adding a hack that simulates this behaviour until a proper solution is
provided.

Apply hack also when DRM_IOCTL_WAIT_VBLANK fails for other reasons and returns
EINVAL (e.g. on Rpi4 with VC4 drm drivers).

Signed-off-by: Esben Haabendal <esben@geanix.com>
Signed-off-by: Francesco Pham <francesco.pham@huawei.com>
Upstream-Status: Pending

diff --git a/xf86drm.c b/xf86drm.c
index b49d42f70dbe..ab8bb563c344 100644
--- a/xf86drm.c
+++ b/xf86drm.c
@@ -2187,6 +2187,15 @@ drm_public int drmWaitVBlank(int fd, drmVBlankPtr vbl)
                    break;
            }
        }
+       else if (ret && (errno == ENOTSUP || errno == EINVAL)) {
+           /* Simulate VBLANK @ 60Hz when DRM driver does not support VBLANK */
+           long delay_us;
+           clock_gettime(CLOCK_MONOTONIC, &cur);
+           delay_us = (cur.tv_nsec % (1000000000L / 60)) / 1000;
+           if (delay_us >= 10) // let's not wait less than 10 us
+               usleep(delay_us);
+           return 0;
+       }
     } while (ret && errno == EINTR);
 
 out:
