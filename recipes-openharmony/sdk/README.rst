.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. _OpenHarmony SDK:

OpenHarmony SDK
###############

.. warning::

    This entire support is under active development and is currently deemed as
    EXPERIMENTAL/EARLY ACCESS.

    As part of the project's effort to improve integration with OpenHarmony
    using a Yocto/OE/Oniro SDK, the team started to work on supporting SDK
    builds that can be used directly in OpenHarmony builds. The current known
    limitation is that at the current stage, this support was only validated
    with the distro configuration, machine and layers setup provided by the
    `meta-openharmony repository/layer <https://gitlab.eclipse.org/eclipse/oniro-oh/meta-openharmony>`_.

    For more information checkout the
    `meta-openharmony README file <https://gitlab.eclipse.org/eclipse/oniro-oh/meta-openharmony/-/blob/kirkstone/README.md>`_.

Oniro Project provides support for building an alternative open-source toolchain
for use with the OpenHarmony reference implementation. This allows using an
updated Clang/LLVM compiler built from the meta-clang Yocto layer instead of the
default prebuilt Clang/LLVM compiler provided by OpenHarmony project.

There are two different Yocto recipes providing prebuilt SDK images for the
OpenHarmony build system.

``oniro-openharmony-toolchain``
  The `oniro-openharmony-toolchain` recipe builds an image containing the Oniro
  Clang version and a musl libc version. The musl libc version is same upstream
  version as currently used in OpenHarmony, and is patched to exhibit the same
  runtime behavior to the musl libc version included in OpenHarmony build
  system.

``oniro-openharmony-bundle``
  The `oniro-openharmony-bundle` recipe is a superset of
  `oniro-openharmony-toolchain`, adding third-party components on top using
  Oniro (Yocto) recipes, replacing the corresponding third-party components
  included in OpenHarmony repository.

Using one of these images replaces then OpenHarmony versions of the included
OpenSource toolchain and third-party components with newer Oniro versions.


Building
********

To build `oniro-openharmony-bundle` for OpenHarmony 3.0.1, an initialized build
environment is required::

    TEMPLATECONF=../meta-openharmony/conf source oe-core/oe-init-build-env

Once that is in place, the `bitbake` command is simply::

    DISTRO=oniro-openharmony-linux MACHINE=qemuarma7 bitbake oniro-openharmony-bundle

In order to build for OpenHarmony 3.1.1 instead, you need to add the following
line to the `build/conf/local.conf` file::

    OPENHARMONY_VERSION = "3.1"

And then use the same command as shown above for 3.0.1.

To build `oniro-openharmony-toolchain` instead, simply use::

    DISTRO=oniro-openharmony-linux MACHINE=qemuarma7 bitbake oniro-openharmony-toolchain


Usage
*****

To use one of the images produced with these recipes, you need to install it to
an OpenHarmony source repository.

Warning! It is recommended to only install OpenHarmony prebuilts to clean
upstream OpenHarmony source repsitories, as the installation will remove files
and entire git repositories!

Firstly, you need to download an OpenHarmony archive. As an example, the next
steps will use OpenHarmony 3.0.1. You are to download this archive from
https://repo.huaweicloud.com/harmonyos/os/3.0.1/code-v3.0.1-LTS.tar.gz and
dearchive it:

.. code-block:: console

    $ tar xfz $DOWNLOADS/code-v3.0.1-LTS.tar.gz

Note that the `$DOWNLOADS` variable needs to point to the location where the
archive resides.

To install the `oniro-openharmony-bundle` to a clean OpenHarmony 3.0.1
repository, you should do something like this:

.. code-block:: console

    $ cd code-v3.0.1-LTS/OpenHarmony
    $ $DOWNLOADS/oniro-openharmony-bundle-3.0-cortexa7-neon-vfpv4-1.99.99.sh -y -d oniro
    $ ./oniro/setup.sh

After this, you can use normal OpenHarmony build system procedures to build as
usual.  To build image for HiSilicon Hi3516DV300 (taurus) board:

.. code-block:: console

    $ ./build.sh --product-name Hi3516DV300
